# +
index_to_split = int(data.shape[0] * .8)

data_M = data[:index_to_split]
data_N = data[index_to_split:]

print(data_M[:, :2].mean(axis=0),"\n", data_M[:, :2].std(axis=0))
print(data_N[:, :2].mean(axis=0),"\n", data_N[:, :2].std(axis=0))
